variable "queue_name" {
  description = "Resource name."
}

variable "delay" {
  description = "Queue Delay in sec."
}

variable "region" {
  description = "AWS Region."
}

variable "bucket_name" {
  description = "S3 Bucket name."
}
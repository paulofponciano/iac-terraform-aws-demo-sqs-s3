resource "random_string" "s3_suffix" {
  length  = 4
  upper = false
  special = false
}

module "s3_bucket" {
  source        = "terraform-aws-modules/s3-bucket/aws"
  version       = "3.13.0"
  create_bucket = true

  bucket = "${random_string.s3_suffix.result}-${var.bucket_name}"

  versioning = {
    enabled = false
  }
}

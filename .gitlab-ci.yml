image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest

workflow:
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
      variables:
        ENV: "production"
    - if: $CI_COMMIT_BRANCH == "development"
      variables:
        ENV: "development"
    - if: $CI_COMMIT_BRANCH == "staging"
      variables:
        ENV: "staging"

variables:
  TF_ROOT: ${CI_PROJECT_DIR}
  TF_ADDRESS: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${ENV}_tf_state

cache:
  key: ${ENV}_tf_state
  paths:
    - ${TF_ROOT}/.terraform

before_script:
  - cd ${TF_ROOT}

stages:
  - init
  - validate
  - build
  - deploy
  - cleanup

init_dev:
  stage: init
  script:
    - gitlab-terraform init
  environment:
    name: development
  only:
    - development 

validate_dev:
  stage: validate
  script:
    - gitlab-terraform validate
  environment:
    name: development
  only:
    - development

plan_dev:
  stage: build 
  script:
    - gitlab-terraform plan -var-file=${ENV}.tfvars 
    - gitlab-terraform plan-json -var-file=${ENV}.tfvars
  artifacts:
    name: plan
    paths:
      - ${TF_ROOT}/plan.cache
    reports:
      terraform:  ${TF_ROOT}/plan.json
  environment:
    name: development
  only:
    - development

apply_dev:
  stage: deploy
  script:
    - gitlab-terraform apply
  dependencies:
    - plan_dev
  environment:
    name: development
  only:
    - development

destroy_dev:
  stage: cleanup
  script:
    - gitlab-terraform destroy -var-file=${ENV}.tfvars
  dependencies:
    - plan_dev
    - apply_dev
  when: manual
  environment:
    name: development
  only:
    - development

init_qas:
  stage: init
  script:
    - gitlab-terraform init
  environment:
    name: staging
  only:
    - staging 

validate_qas:
  stage: validate
  script:
    - gitlab-terraform validate
  environment:
    name: staging
  only:
    - staging

plan_qas:
  stage: build 
  script:
    - gitlab-terraform plan -var-file=${ENV}.tfvars 
    - gitlab-terraform plan-json -var-file=${ENV}.tfvars
  artifacts:
    name: plan
    paths:
      - ${TF_ROOT}/plan.cache
    reports:
      terraform:  ${TF_ROOT}/plan.json
  environment:
    name: staging
  only:
    - staging

apply_qas:
  stage: deploy
  script:
    - gitlab-terraform apply
  dependencies:
    - plan_qas
  environment:
    name: staging
  only:
    - staging

destroy_qas:
  stage: cleanup
  script:
    - gitlab-terraform destroy -var-file=${ENV}.tfvars
  dependencies:
    - plan_qas
    - apply_qas
  when: manual
  environment:
    name: staging
  only:
    - staging

init_prd:
  stage: init
  script:
    - gitlab-terraform init
  environment:
    name: default
  only:
    - main 

validate_prd:
  stage: validate
  script:
    - gitlab-terraform validate
  environment:
    name: default
  only:
    - main

plan_prd:
  stage: build
  script:
    - gitlab-terraform plan -var-file=${ENV}.tfvars 
    - gitlab-terraform plan-json -var-file=${ENV}.tfvars
  artifacts:
    name: plan
    paths:
      - ${TF_ROOT}/plan.cache
    reports:
      terraform:  ${TF_ROOT}/plan.json
  environment:
    name: default
  only:
    - main    

apply_prd:
  stage: deploy
  script:
    - gitlab-terraform apply
  dependencies:
    - plan_prd
  environment:
    name: default
  only:
    - main

destroy_prd:
  stage: cleanup
  script:
    - gitlab-terraform destroy -var-file=${ENV}.tfvars
  dependencies:
    - plan_prd
    - apply_prd
  when: manual
  environment:
    name: default
  only:
    - main